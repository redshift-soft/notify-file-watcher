﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notify
{
    public static class Messages
    {
        public static readonly string error_header = "Error!";
        public static readonly string start_argument_exception_error = "Error in file path.Make sure the application is pointed to a valid file";
        public static readonly string tray_icon_hover_text = "Notify File Watcher";
        public static readonly string tray_icon_balloon_tip_text = "Notify is still running";
    }
}
