﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Windows.Forms;


namespace Notify
{
    public partial class Form1 : Form
    {
        bool _iswatching;
        int _timerinterval = 2000;
        string filepath;
        string filename;
        DateTime lastwritetime;
        FileSystemWatcher watcher = new FileSystemWatcher();
        System.Timers.Timer timer = new System.Timers.Timer();
        Dictionary<string, string> carriers = new Dictionary<string, string>()
        {
            { "AT&T", "txt.att.net" },
            { "Verizon", "vtext.com" },
            { "Sprint", "messaging.sprintpcs.com" },
            { "T-Mobile (USA)", "tmomail.net" }
        };
        public Form1()
        {
            InitializeComponent();
            _iswatching = false;
            SetWatcherStatus();
            SetToggleButtonText();
            PopulateCarrierComboBox();

            timer.Interval = _timerinterval;
            timer.Elapsed += new System.Timers.ElapsedEventHandler(timer_Tick);
        }
        public bool IsWatching
        {
            get { return this._iswatching; }
            set
            {
                this._iswatching = value;
                if (this._iswatching)
                {
                    try
                    {
                        timer.Start();
                    }
                    catch (System.ArgumentException start_exception)
                    {
                        this._iswatching = false;
                        MessageBox.Show(Messages.start_argument_exception_error, Messages.error_header);
                        timer.Stop();
                    }
                }
                else
                    watcher.EnableRaisingEvents = false;
                SetWatcherStatus();
                SetToggleButtonText();
            }
        }
        private void SetWatcherStatus()
        {
            if (!_iswatching)
            {
                watcher_status.ForeColor = Color.Red;
                watcher_status.Text = "Not Watching";
            }
            else
            {
                watcher_status.ForeColor = Color.Green;
                watcher_status.Text = "Watching";
            }
        }
        private void SetToggleButtonText()
        {
            if (!_iswatching)
                function_toggle.Text = "Start";
            else
                function_toggle.Text = "Stop";
        }
        private void PopulateCarrierComboBox()
        {
            foreach(KeyValuePair<string, string> kvp in carriers)
            {
                ComboBoxItem _item = new ComboBoxItem()
                {
                    Text = kvp.Key.ToString(),
                    Value = kvp.Value.ToString()
                };
                carrier_bx.Items.Add(_item);
            }
        }
        //Events
        private void choose_btn_Click(object sender, EventArgs e)
        {
            if(openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                filepath = openFileDialog1.FileName;
                filename = Path.GetFileName(filepath);
                lastwritetime = File.GetLastWriteTime(filepath);               
                file_txtbx.Text = filepath;
            }
        }
        private void Notify()
        {
            string v = null;
            this.Invoke(new MethodInvoker(delegate () {
                v = (carrier_bx.SelectedItem as ComboBoxItem).Value.ToString();
            }));


            string smtp_server = smtp_txtbx.Text;
            int port = 587;
            string username = smtp_user_txtbx.Text;
            string password = smtp_pwd_txtbx.Text;

            string sms_sender = "noone@nowhere.com";
            string recipient = ph_num_txtbx.Text + "@" + v;
            string subject = "File Changed";
            string message = String.Concat(new object[]
            {
                "File:  ",
                filename,
                " has changed"
            });
            using (MailMessage textMessage = new MailMessage(sms_sender, recipient, subject, message))
            {
                using (SmtpClient textMessageClient = new SmtpClient(smtp_server, port))
                {
                    textMessageClient.UseDefaultCredentials = false;
                    textMessageClient.EnableSsl = true;
                    if (user_pass_toggle.CheckState == CheckState.Checked)
                        textMessageClient.Credentials = new NetworkCredential(username, password);
                    try
                    {
                        textMessageClient.Send(textMessage);
                    }
                    catch (Exception send_exception)
                    {
                        MessageBox.Show(send_exception.Message, Messages.error_header);
                    }
                }
            }
        }
        private void user_pass_toggle_CheckedChanged(object sender, EventArgs e)
        {
            if (panel1.Visible) panel1.Visible = false;
            else panel1.Visible = true;
        }
        private void exitToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Application.Exit();
        }
        private void function_toggle_Click(object sender, EventArgs e)
        {
            if (!_iswatching)
                IsWatching = true;
            else
                IsWatching = false;
        }
        private void timer_Tick (object sender, System.Timers.ElapsedEventArgs e)
        {
            DateTime _dt = File.GetLastWriteTime(filepath);
            if(lastwritetime != _dt)
            {
                lastwritetime = _dt;
                Notify();
            }
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            tray_icon.BalloonTipText = Messages.tray_icon_balloon_tip_text;
            if (FormWindowState.Minimized == this.WindowState)
            {
                tray_icon.Visible = true;
                tray_icon.ShowBalloonTip(5000);
                tray_icon.Text = Messages.tray_icon_hover_text;
                this.Hide();
            }
            else if (FormWindowState.Normal == this.WindowState)
            {
                tray_icon.Visible = false;
            }
        }

        private void tray_icon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
        }
    }
    public class ComboBoxItem
    {
        public string Text { get; set; }
        public string Value { get; set; }
        public override string ToString()
        {
            return Text;
        }
    }
}
