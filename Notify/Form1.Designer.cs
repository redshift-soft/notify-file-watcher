﻿namespace Notify
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.choose_btn = new System.Windows.Forms.Button();
            this.file_txtbx = new System.Windows.Forms.TextBox();
            this.choose_lbl = new System.Windows.Forms.Label();
            this.ph_num_txtbx = new System.Windows.Forms.TextBox();
            this.ph_num_lbl = new System.Windows.Forms.Label();
            this.smtp_txtbx = new System.Windows.Forms.TextBox();
            this.smtp_lbl = new System.Windows.Forms.Label();
            this.user_pass_toggle = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.smtp_pwd_lbl = new System.Windows.Forms.Label();
            this.smtp_pwd_txtbx = new System.Windows.Forms.TextBox();
            this.user_smtp_lbl = new System.Windows.Forms.Label();
            this.smtp_user_txtbx = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.watcher_status_lbl = new System.Windows.Forms.Label();
            this.watcher_status = new System.Windows.Forms.Label();
            this.function_toggle = new System.Windows.Forms.Button();
            this.carrier_bx = new System.Windows.Forms.ComboBox();
            this.car_lbl = new System.Windows.Forms.Label();
            this.tray_icon = new System.Windows.Forms.NotifyIcon(this.components);
            this.panel1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // choose_btn
            // 
            this.choose_btn.Location = new System.Drawing.Point(275, 77);
            this.choose_btn.Name = "choose_btn";
            this.choose_btn.Size = new System.Drawing.Size(75, 23);
            this.choose_btn.TabIndex = 1;
            this.choose_btn.Text = "Choose";
            this.choose_btn.UseVisualStyleBackColor = true;
            this.choose_btn.Click += new System.EventHandler(this.choose_btn_Click);
            // 
            // file_txtbx
            // 
            this.file_txtbx.Location = new System.Drawing.Point(16, 79);
            this.file_txtbx.Name = "file_txtbx";
            this.file_txtbx.Size = new System.Drawing.Size(253, 20);
            this.file_txtbx.TabIndex = 2;
            // 
            // choose_lbl
            // 
            this.choose_lbl.AutoSize = true;
            this.choose_lbl.Location = new System.Drawing.Point(16, 60);
            this.choose_lbl.Name = "choose_lbl";
            this.choose_lbl.Size = new System.Drawing.Size(112, 13);
            this.choose_lbl.TabIndex = 3;
            this.choose_lbl.Text = "Choose a file to watch";
            // 
            // ph_num_txtbx
            // 
            this.ph_num_txtbx.Location = new System.Drawing.Point(16, 131);
            this.ph_num_txtbx.Name = "ph_num_txtbx";
            this.ph_num_txtbx.Size = new System.Drawing.Size(234, 20);
            this.ph_num_txtbx.TabIndex = 4;
            // 
            // ph_num_lbl
            // 
            this.ph_num_lbl.AutoSize = true;
            this.ph_num_lbl.Location = new System.Drawing.Point(19, 112);
            this.ph_num_lbl.Name = "ph_num_lbl";
            this.ph_num_lbl.Size = new System.Drawing.Size(211, 13);
            this.ph_num_lbl.TabIndex = 5;
            this.ph_num_lbl.Text = "Choose number to text (AT&T numbers only):";
            // 
            // smtp_txtbx
            // 
            this.smtp_txtbx.Location = new System.Drawing.Point(16, 184);
            this.smtp_txtbx.Name = "smtp_txtbx";
            this.smtp_txtbx.Size = new System.Drawing.Size(234, 20);
            this.smtp_txtbx.TabIndex = 6;
            // 
            // smtp_lbl
            // 
            this.smtp_lbl.AutoSize = true;
            this.smtp_lbl.Location = new System.Drawing.Point(19, 164);
            this.smtp_lbl.Name = "smtp_lbl";
            this.smtp_lbl.Size = new System.Drawing.Size(139, 13);
            this.smtp_lbl.TabIndex = 7;
            this.smtp_lbl.Text = "Specify your SMTP server:  ";
            // 
            // user_pass_toggle
            // 
            this.user_pass_toggle.AutoSize = true;
            this.user_pass_toggle.Location = new System.Drawing.Point(36, 213);
            this.user_pass_toggle.Name = "user_pass_toggle";
            this.user_pass_toggle.Size = new System.Drawing.Size(203, 17);
            this.user_pass_toggle.TabIndex = 8;
            this.user_pass_toggle.Text = "SMTP Requires Username/Password";
            this.user_pass_toggle.UseVisualStyleBackColor = true;
            this.user_pass_toggle.CheckedChanged += new System.EventHandler(this.user_pass_toggle_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.smtp_pwd_lbl);
            this.panel1.Controls.Add(this.smtp_pwd_txtbx);
            this.panel1.Controls.Add(this.user_smtp_lbl);
            this.panel1.Controls.Add(this.smtp_user_txtbx);
            this.panel1.Location = new System.Drawing.Point(62, 237);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(288, 95);
            this.panel1.TabIndex = 9;
            this.panel1.Visible = false;
            // 
            // smtp_pwd_lbl
            // 
            this.smtp_pwd_lbl.AutoSize = true;
            this.smtp_pwd_lbl.Location = new System.Drawing.Point(21, 45);
            this.smtp_pwd_lbl.Name = "smtp_pwd_lbl";
            this.smtp_pwd_lbl.Size = new System.Drawing.Size(62, 13);
            this.smtp_pwd_lbl.TabIndex = 3;
            this.smtp_pwd_lbl.Text = "Password:  ";
            // 
            // smtp_pwd_txtbx
            // 
            this.smtp_pwd_txtbx.Location = new System.Drawing.Point(21, 61);
            this.smtp_pwd_txtbx.Name = "smtp_pwd_txtbx";
            this.smtp_pwd_txtbx.PasswordChar = '*';
            this.smtp_pwd_txtbx.Size = new System.Drawing.Size(186, 20);
            this.smtp_pwd_txtbx.TabIndex = 2;
            // 
            // user_smtp_lbl
            // 
            this.user_smtp_lbl.AutoSize = true;
            this.user_smtp_lbl.Location = new System.Drawing.Point(21, 3);
            this.user_smtp_lbl.Name = "user_smtp_lbl";
            this.user_smtp_lbl.Size = new System.Drawing.Size(64, 13);
            this.user_smtp_lbl.TabIndex = 1;
            this.user_smtp_lbl.Text = "Username:  ";
            // 
            // smtp_user_txtbx
            // 
            this.smtp_user_txtbx.Location = new System.Drawing.Point(21, 22);
            this.smtp_user_txtbx.Name = "smtp_user_txtbx";
            this.smtp_user_txtbx.Size = new System.Drawing.Size(186, 20);
            this.smtp_user_txtbx.TabIndex = 0;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(395, 24);
            this.menuStrip1.TabIndex = 10;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(37, 20);
            this.toolStripMenuItem1.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click_1);
            // 
            // watcher_status_lbl
            // 
            this.watcher_status_lbl.AutoSize = true;
            this.watcher_status_lbl.Location = new System.Drawing.Point(13, 31);
            this.watcher_status_lbl.Name = "watcher_status_lbl";
            this.watcher_status_lbl.Size = new System.Drawing.Size(88, 13);
            this.watcher_status_lbl.TabIndex = 11;
            this.watcher_status_lbl.Text = "Watcher status:  ";
            // 
            // watcher_status
            // 
            this.watcher_status.AutoSize = true;
            this.watcher_status.Location = new System.Drawing.Point(108, 31);
            this.watcher_status.Name = "watcher_status";
            this.watcher_status.Size = new System.Drawing.Size(23, 13);
            this.watcher_status.TabIndex = 12;
            this.watcher_status.Text = "null";
            // 
            // function_toggle
            // 
            this.function_toggle.Location = new System.Drawing.Point(275, 338);
            this.function_toggle.Name = "function_toggle";
            this.function_toggle.Size = new System.Drawing.Size(75, 23);
            this.function_toggle.TabIndex = 13;
            this.function_toggle.Text = "button1";
            this.function_toggle.UseVisualStyleBackColor = true;
            this.function_toggle.Click += new System.EventHandler(this.function_toggle_Click);
            // 
            // carrier_bx
            // 
            this.carrier_bx.FormattingEnabled = true;
            this.carrier_bx.Location = new System.Drawing.Point(256, 131);
            this.carrier_bx.Name = "carrier_bx";
            this.carrier_bx.Size = new System.Drawing.Size(121, 21);
            this.carrier_bx.TabIndex = 14;
            // 
            // car_lbl
            // 
            this.car_lbl.AutoSize = true;
            this.car_lbl.Location = new System.Drawing.Point(257, 112);
            this.car_lbl.Name = "car_lbl";
            this.car_lbl.Size = new System.Drawing.Size(101, 13);
            this.car_lbl.TabIndex = 15;
            this.car_lbl.Text = "Choose Your Carrier";
            // 
            // tray_icon
            // 
            this.tray_icon.Icon = ((System.Drawing.Icon)(resources.GetObject("tray_icon.Icon")));
            this.tray_icon.Text = "tray_icon";
            this.tray_icon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.tray_icon_MouseDoubleClick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(395, 371);
            this.Controls.Add(this.car_lbl);
            this.Controls.Add(this.carrier_bx);
            this.Controls.Add(this.function_toggle);
            this.Controls.Add(this.watcher_status);
            this.Controls.Add(this.watcher_status_lbl);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.user_pass_toggle);
            this.Controls.Add(this.smtp_lbl);
            this.Controls.Add(this.smtp_txtbx);
            this.Controls.Add(this.ph_num_lbl);
            this.Controls.Add(this.ph_num_txtbx);
            this.Controls.Add(this.choose_lbl);
            this.Controls.Add(this.file_txtbx);
            this.Controls.Add(this.choose_btn);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Notify";
            this.Resize += new System.EventHandler(this.Form1_Resize);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button choose_btn;
        private System.Windows.Forms.TextBox file_txtbx;
        private System.Windows.Forms.Label choose_lbl;
        private System.Windows.Forms.TextBox ph_num_txtbx;
        private System.Windows.Forms.Label ph_num_lbl;
        private System.Windows.Forms.TextBox smtp_txtbx;
        private System.Windows.Forms.Label smtp_lbl;
        private System.Windows.Forms.CheckBox user_pass_toggle;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label smtp_pwd_lbl;
        private System.Windows.Forms.TextBox smtp_pwd_txtbx;
        private System.Windows.Forms.Label user_smtp_lbl;
        private System.Windows.Forms.TextBox smtp_user_txtbx;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.Label watcher_status_lbl;
        private System.Windows.Forms.Label watcher_status;
        private System.Windows.Forms.Button function_toggle;
        private System.Windows.Forms.ComboBox carrier_bx;
        private System.Windows.Forms.Label car_lbl;
        private System.Windows.Forms.NotifyIcon tray_icon;
    }
}

